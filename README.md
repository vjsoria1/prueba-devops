# Prueba DevOps Tottus

Prueba diseñada para ver tus habilidades en el mundo DevOps. La prueba consiste en 4 etapas donde se evaluará las herramientas fundamentales que utilizamos tales como docker, kubernetes, CI/CD y terraform. 

## Fase del proceso de selección:
  1. En caso de que cumplas con el perfil técnico del cargo, la primera fase del proceso de selección es uan entrevista tecnica, con el Lider del area Devops.
  2. Como segunda fase comtemplamos una pequeña prueba tecnica, es importante que realices dicha prueba en forma tranquila (tendrás 2 días máximo para poder enviarlo). No te preocupes sino puedes completar todas las fases, para nosotros es importante que realices lo que consideras que tienes experiencia.
  3. Si continúas avanzando con nosotros, el próximo paso es un acercamiento final al equipo con el que estara trabajando, y bienvenida a la familia devops tottus

Una vez completado, no olvide notificar la solución mnlara@tottus.cl

Si tienes alguna duda, puedes escribir a Marvis Lara o enviar un correo electronico a la persona de contacto de tu proveedor.

¡Te deseamos mucho éxito!

## La aplicación
![NodeJs](./img/nodejs.png)

### Instalar Dependencias
```bash
$ npm install
npm WARN deprecated request-promise-native@1.0.9: request-promise-native has been deprecated because it extends the now deprecated request package, see https://github.com/request/request/issues/3142
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated har-validator@5.1.5: this library is no longer supported
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^2.1.2 (node_modules/jest-haste-map/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.1: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN basicservice@1.0.0 No repository field.

added 557 packages from 359 contributors and audited 558 packages in 29.142s

20 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
### Ejecutar Test

```bash
$ npm run test

> basicservice@1.0.0 test /basic-unit-test
> jest

 PASS  tests/string.test.js
 PASS  reto/tests/sum.test.js
 PASS  tests/sum.test.js
 PASS  reto/tests/string.test.js

Test Suites: 4 passed, 4 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        3.052s
Ran all test suites.
```

### Ejecutar la aplicación

```bash
$ node index.js
Example app listening on port 3000!
```
Podrá acceder a la API localmente en el puerto `3000`.

```bash
$ curl http://localhost:3000/ -s | jq
{
  "msg": "Easy peasy lemon squeezy ;)"
}
$ curl http://localhost:3000/cheers -s | jq
{
  "msg": "You can do it :)"
}
$ curl http://localhost:3000/private -s | jq
{
  "private_token": "VmFzIG11eSBiaWVuIQo="
}
```

### Importante
Las aplicaciones entregadas por el equipo de desarrollo deben venir bien documentada para que el equipo de **DevOps** pueda realizar todo el flujo CI/CD más rápido.

También nos parece superimportante dejar nuestro trabajo respaldado, por lo que dejar bien documentada la solución de la prueba te dará puntos extras.

## Etapa 1 Contenedores
![docker](./img/docker.jpeg)

Los contenedores es la base principal de nuestras aplicaciones, además de que es una tecnología emergente que está haciendo grandes cambios. Esta etapa consiste en demostrar tus conocimientos en contenedores(docker específicamente) y hacer lo siguiente:

  1. Crear un dockerfile que generé la imagen más pequeña y segura posible. 
  # Listo
  2. Desplegar la aplicación y probarla en los puertos 3000 y 2375.
  # docker run -d -p 3000:3000 -it vjsoria01/prueba-devops:ejercicio1
  # El puerto 2375 es usado por docker
  3. Coméntanos tu experiencia de cada paso y cuales fueron los resultados obtenidos.
  # Se comprobo que la funcionalidad funcionaba correctamente exponiendo el puerto 3000, sin embargo no se pudo exponer en el puerto 2375 debido a que es un puerto usado por Docker.

## Etapa 2 Kubernetes
![k8s](./img/kubernetes.jpeg)

Luego de crear tu contenedor con la aplicación te invitamos a orquestarlo con k8s. Los pasos para completar esta etapa son:

  1. Utiliza minikube o microk8s para la elaboración de esta etapa, esto nos permitirá corregirla más rápidamente.
  # Se uso un cluster gratuito de IBM cloud
  2. Crear los manifiestos necesarios para desplegar la aplicación y poder probarla. Documenta los yaml y como lograste consumir la aplicación.
  # Se uso el comando helm create para generar los manifiestos y se los modificaron segun fue conveniente.
  3. Luego de probar los manifiestos, utiliza helm para empaquetar tu solución y pruébala de esta forma.
  # Ya se tenia armado el chart, se procedio a instalar el aplicativo en dos namespaces distintos.
  4. **BONUS**: Cuéntanos que es lo que más te gusta de kubernetes y si pudieras, ¿qué le cambiarias?
  # Nos permite concentrarnos en el desarrollo de la aplicacion, delegando muchos temas al orquestador, como los healt checks, el autohealing, etc.

## Etapa 3 Terraform
![terraform](./img/terraform.png)

La creación de infraestructura como código es fundamental en nuestros proyectos, ya que son creados con terraform. En esta etapa debes construir lo siguiente:

 1. Un rol personalizado en kubernetes.
 2. Una cuenta de servicio asociada al rol en kubernetes y que tenga permisos sobre un namespace en particular. 
 3. **BONUS**: Demuéstranos como crearías el cluster k8s más pequeño posible en tu nube favorita! Justifica el tamaño que elegiste. 

## Etapa 4 Construcción de CI/CD
![cicd](./img/cicd.jpeg)

La automatización es una de nuestras principales pasiones y utilizar las herramientas de **CI/CD** es parte de nuestro día a día. Esta etapa consiste en lo siguiente:

1. Crea un flujo exitoso para la aplicación utilizando la herramienta que mejor domines o consideres.
2. Automatiza la etapa 3, logrando crear infraestructura de forma segura y automatizada.
3. **BONUS**: Utilizar Gitlab-CI te dará puntos extra, ya que es nuestra herramienta de trabajo actual, adicionalmente para esto te pedimos que instales un runner de gitlab en tu clúster creado en los pasos anteriores. ¡Cuéntanos como te fue con esto! 


¡Te deseamos el mejor de los éxitos!
