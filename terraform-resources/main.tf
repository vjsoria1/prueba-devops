terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}
provider "kubernetes" {
  config_path = "~/.kube/config"
}
resource "kubernetes_namespace" "test" {
  metadata {
    name = "prueba-devops-tf"
  }
}

resource "kubernetes_service_account" "test" {
  metadata {
    name = "prueba-sa"
    namespace = "prueba-devops-tf"
  }
}

resource "kubernetes_role" "test" {
  metadata {
    name = "prueba-role"
    namespace = "prueba-devops-tf"
  }

  rule {
    api_groups     = [""]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "test" {
  metadata {
    name      = "prueba-role-binding"
    namespace = "prueba-devops-tf"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "prueba-role"
  }
  subject {
    kind      = "ServiceAccount"
    name      = "prueba-sa"
    namespace = "prueba-devops-tf"
  }
}